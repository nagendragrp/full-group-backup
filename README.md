# Full group export

Combines the group export API and the project export API to export:
1. A group and all its subgroups
2. All projects contained within that group.

Restrictions to the exports as documented on the API pages apply - so this export is only as "full" as the APIs allow.

## Usage

`pip3 install -r requirements.txt`

After importing $GIT_TOKEN in the environment:

`python3 group_export.py GIT_TOKEN $GROUP_ID`

### Parameters

* `GIT_TOKEN`: Name of an environment variable containing an API token of a group owner. Needs `api` scope.
* `GROUP_ID`: ID or path of a GitLab group
* `--gitlab` (Optional): URL of the GitLab you want to export from. Defaults to https://gitlab.com

## Disclaimer

This script is provided for educational purposes. It is not supported by GitLab. However, you can create an issue if you need help or better propose a Merge Request. This script collects audit events which may hold confidential business and/or user data. Please use this script and its outputs with caution and in accordance to your local data privacy regulations.
